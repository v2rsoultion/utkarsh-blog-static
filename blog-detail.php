<!-- Header --> <?php include 'header.php';?>
<!-- Blog Banner -->
<section class="inner-page-banner">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <div>
          <h1>CLAT Eligibility 2020 – Age Limit, Academic Qualification</h1>
          <ul>
            <li title="Home"><a href="javascript:void(0)">Home</a></li>
            <li>/</li>
            <li title="CLAT Eligibility 2020 – Age Limit, Academic Qualification">
              <a href="javascript:void(0)">CLAT Eligibility 2020 – Age Limit, Academic Qualification</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End Blog Banner -->
<!-- Blog detail Section start -->
<section>
  <div class="container">
    <div class="row">
      <div class="col-xl-8 col-lg-8 col-md-6">
        <div class="blog-heading">
          <img src="images/blog-low-img.jpg" alt="Image" class="img-fluid" />
          <div class="blog-detail-box detail-page-box">
            <ul>
              <li><i class="fas fa-user"></i> Utkarsh</li>
              <li><i class="far fa-calendar-alt"></i> February 1, 2020</li>
              <li><i class="fas fa-comment-dots"></i> 2</li>
              <li>
                <i class="fas fa-external-link-square-alt"></i> Blog, Blog English, Other
              </li>
            </ul>
            <h5> CLAT Eligibility 2020 – Age Limit, Academic Qualification </h5>
            <p> CLAT stands for Common Law Admission Test and is a national-level entrance exam conducted by the
              Consortium of the National Law Universities of India to provide admissions in various law colleges in the
              country. The new date for the CLAT exam has been released, and now the exam will be conducted on 22 August
              2020. </p>
            <p> The registrations for the exam were extended until 10 July 2020. The candidates who wish to apply for
              the CLAT exam must fulfil the eligibility criteria to appear for the exam. The eligibility criteria vary
              as per the course and participating colleges. We have discussed the complete eligibility criteria for CLAT
              2020 in this article to help the students know the parameters. </p>
            <div class="program-detail">
              <h4>Undergraduate Program</h4>
              <p> The candidates who are applying for CLAT to get admission in undergraduate courses must meet the
                following parameters to be eligible for the exam: </p>
              <h6>Age Limit</h6>
              <ul>
                <li> As per the new, updated guidelines by the NUSRL, Ranchi, there is no upper age limit applicable in
                  CLAT 2020. </li>
                <li> However, the age of the candidates will play a determining role if a tie exists between two
                  candidates during the results. </li>
              </ul>
              <h6>Educational Qualification</h6>
              <ul>
                <li> The candidates applying for CLAT must have cleared class XII with a minimum of 45% marks
                  (Applicable for unreserved/OBC/Specially Abled Persons and other categories). </li>
                <li> The candidates belonging to Scheduled Caste (SC)/Scheduled Tribe (ST) category must have cleared
                  class XII with a minimum of 40% marks. </li>
                <li> The candidates appearing in the Class XII examination are also eligible to apply for the exam, but
                  their admission will be subjective to the provision of their result at the time of the CLAT Admission.
                </li>
                <li> The candidates who fail to provide proof of passing Class XII will not be eligible. </li>
              </ul>
              <h6>General Criteria</h6>
              <ul>
                <p> In case a tie occurs between two students, the higher rank will be decided by a tie-breaker which is
                  as follows: </p>
                <li> Higher marks in the Legal Aptitude section in CLAT 2020; if the tie still persists, then </li>
                <li>Higher Age; if the tie still persists, then</li>
                <li>Lucky Draw</li>
              </ul>
            </div>
            <div class="program-detail">
              <h4 class="mb-2">Postgraduate Program</h4>
              <h6>Age Limit</h6>
              <ul>
                <li> There is no upper age limit applicable in CLAT 2020for the postgraduate programs. </li>
              </ul>
              <h6>Educational Qualification</h6>
              <ul>
                <li> The candidates applying for CLAT for the post-graduation program should possess an LLB (Legum
                  Baccalaureus) degree or equivalent with a minimum of 50% marks. (Applicable for
                  General/OBC/PwD/NRI/PIO/OCI students). </li>
                <li> The candidates applying for CLAT for the post-graduation program should possess an LLB (Legum
                  Baccalaureus) degree or equivalent with a minimum of 50% marks. (Applicable for SC/STstudents). </li>
                <li> Candidates in their final year (appearing in LLB Final Year exam or equivalent) are also eligible
                  to apply for CLAT. </li>
              </ul>
              <h6>General Criteria</h6>
              <ul>
                <p> In case a tie occurs between two students, the higher rank will be decided by a tie-breaker which is
                  as follows: </p>
                <li>Higher Age; if the tie still persists, then</li>
                <li>Lucky Draw</li>
              </ul>
            </div>
            <div class="program-detail">
              <h4>CLAT Eligibility for NRI candidates</h4>
              <p> CLAT offers the opportunity to NRI candidates to get admission in various Law Colleges in India purely
                on the basis of merit in the CLAT exam. The following pointers must be considered by the NRI students:
              </p>
              <ul>
                <li> The NRI candidates should directly reach the university to get information regarding the
                  eligibility criteria, essential documents, and other queries for admission under the NRI/NRI sponsored
                  category. </li>
                <li> The NRI candidates must have a copy of a valid Indian Passport of the Student, a copy of Visa, and
                  a certificate from the local embassy verifying that the individual is an NRI/OCI or PIO. </li>
                <li> At the time of admission, the NRI candidate must present a copy of a valid Indian Passport of the
                  Sponsor, a copy of the Visa obtained by their sponsor, a certificate from the local embassy verifying
                  that the sponsor is an NRI/OCI or PIO Cardholder, and an affidavit by the sponsor indicating they are
                  interested in the education affairs of the student and will fund the education of student over the
                  next 5 years. </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="blog-paging">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <a href="javascripy:void(0)" class="text-left" title="7 Unique Ways to Crack REET Exam in 1 Month">
                <i class="fa fa-chevron-left"></i>
                <span>7 Unique Ways to Crack REET Exam in 1 Month</span>
              </a>
            </div>
            <div class="col-lg-6 col-md-12">
              <a href="javascripy:void(0)" class="text-right"
                title="15 Mistakes to avoid while preparing for Government Exams">
                <span>15 Mistakes to avoid while preparing for Government Exams</span>
                <i class="fa fa-chevron-right"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="comment-box">
          <h5>Comments <span>(2)</span></h5>
          <ul>
            <li>
              <img src="images/comment-user-1.jpg" alt="img" class="img-fluid" />
              <div class="comment-detail">
                <a href="javascriptvoid:(0)" title="Juanita Jones">Juanita Jones </a>
                <button title="Reply" class="reply-comment">Reply</button>
                <span>Monday, July 20, 2020</span>
                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore
                  et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris consequat.
                </p>
                <form class="replyForm">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Type a reply" />
                    <button type="button" title="Send">Send</button>
                  </div>
                </form>
              </div>
            </li>
            <li>
              <img src="images/comment-user.jpg" alt="img" class="img-fluid" />
              <div class="comment-detail">
                <a href="javascriptvoid:(0)" title="Richard King">Richard King </a>
                <button title="Reply" class="reply-comment">Reply</button>
                <span>Monday, July 20, 2020</span>
                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore
                  et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris consequat.
                </p>
                <form class="replyForm">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Type a reply" />
                    <button type="button" title="Send">Send</button>
                  </div>
                </form>
              </div>
            </li>
          </ul>
        </div>
        <div class="commment-form">
          <h5>Leave a Comment</h5>
          <form id="commentForm" method="POST">
            <div class="form-row">
              <div class="form-group col-md-12 col-lg-6">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Enter your name" />
              </div>
              <div class="form-group col-md-12 col-lg-6">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Enter your email" />
              </div>
            </div>
            <div class="form-group">
              <label for="comment">Comment</label>
              <textarea class="form-control" id="comment" placeholder="Enter your comment" rows="4"
                name="commentBox"></textarea>
            </div>
            <div class="form-group">
              <div class="custom-control custom-checkbox">
                <input class="custom-control-input" type="checkbox" id="commentCheck" name="commentCheck" />
                <label class="custom-control-label" for="commentCheck"> Save my name, email, and website in this browser
                  for the next time I comment. </label>
              </div>
            </div>
            <button type="submit" title="Submit Comment" class=""> Submit Comment </button>
          </form>
        </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-6">
        <!-- blog sidebar --> <?php include 'blog-sidebar.php';?>
      </div>
    </div>
  </div>
</section>
<!-- Blog detail Section End -->
<!-- Footer --> <?php include 'footer.php';?>