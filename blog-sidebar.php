<div class="search-box">
  <input type="text" placeholder="Search" />
  <i class="far fa-search search-icon"></i>
</div>
<div class="blog-categories">
  <h5>
    <img src="images/notice-before-img.png" alt="icon" /> Categories
  </h5>
  <ul>
    <li>
      <a href="javascript:void(0)" title="Banking"> Banking </a>
    </li>
    <li>
      <a href="javascript:void(0)" title="Entrance Exam"> Entrance Exam </a>
    </li>
    <li>
      <a href="javascript:void(0)" title="Judical Services"> Judical Services </a>
    </li>
    <li>
      <a href="javascript:void(0)" title="Railway"> Railway </a>
    </li>
    <li>
      <a href="javascript:void(0)" title="Rajasthan Police"> Rajasthan Police </a>
    </li>
    <li>
      <a href="javascript:void(0)" title="REET"> REET </a>
    </li>
    <li>
      <a href="javascript:void(0)" title="RPSC"> RPSC </a>
    </li>
    <li>
      <a href="javascript:void(0)" title="RSSB (RSMSSB)"> RSSB (RSMSSB) </a>
    </li>
    <li>
      <a href="javascript:void(0)" title="UPSC"> UPSC </a>
    </li>
  </ul>
</div>
<div class="blog-categories">
  <h5>
    <img src="images/notice-before-img.png" alt="icon" /> Recent Posts
  </h5>
  <div class="recent-posts">
    <ul>
      <li>
        <img src="images/blog-post-img1.jpg" alt="img" />
        <div class="recent-blog-title">
          <span>August 1, 2020</span><br />
          <a href="javascript:void(0)" title="Self-Study or Professional Coaching Common Dilemma">Self-Study or
            Professional Coaching Common Dilemma</a>
        </div>
      </li>
      <li>
        <img src="images/blog-post-img2.jpg" alt="img" />
        <div class="recent-blog-title">
          <span>August 1, 2020</span><br />
          <a href="javascript:void(0)" title="कैसे करें रीट परीक्षा की तैयारी केवल एक महीने में">कैसे करें रीट परीक्षा
            की तैयारी केवल एक महीने में</a>
        </div>
      </li>
      <li>
        <img src="images/blog-post-img3.jpg" alt="img" />
        <div class="recent-blog-title">
          <span>August 1, 2020</span><br />
          <a href="javascript:void(0)" title="प्रधानमंत्री मुद्रा योजना (PMMY) और मुद्रा ऋण">प्रधानमंत्री मुद्रा योजना
            (PMMY) और मुद्रा ऋण</a>
        </div>
      </li>
    </ul>
  </div>
</div>
<div class="blog-categories">
  <h5>
    <img src="images/notice-before-img.png" alt="icon" /> Popular Posts
  </h5>
  <div class="recent-posts">
    <ul>
      <li>
        <img src="images/blog-post-img1.jpg" alt="img" />
        <div class="recent-blog-title">
          <span>August 1, 2020</span><br />
          <a href="javascript:void(0)" title="Self-Study or Professional Coaching Common Dilemma">Self-Study or
            Professional Coaching Common Dilemma</a>
        </div>
      </li>
      <li>
        <img src="images/blog-post-img2.jpg" alt="img" />
        <div class="recent-blog-title">
          <span>August 1, 2020</span><br />
          <a href="javascript:void(0)" title="कैसे करें रीट परीक्षा की तैयारी केवल एक महीने में">कैसे करें रीट परीक्षा
            की तैयारी केवल एक महीने में</a>
        </div>
      </li>
      <li>
        <img src="images/blog-post-img3.jpg" alt="img" />
        <div class="recent-blog-title">
          <span>August 1, 2020</span><br />
          <a href="javascript:void(0)" title="प्रधानमंत्री मुद्रा योजना (PMMY) और मुद्रा ऋण">प्रधानमंत्री मुद्रा योजना
            (PMMY) और मुद्रा ऋण</a>
        </div>
      </li>
    </ul>
  </div>
</div>
<div class="blog-categories">
  <h5>
    <img src="images/notice-before-img.png" alt="icon" /> Tags
  </h5>
  <div class="blog-tags">
    <a href="javascript:void(0)" title="IAS Exam">IAS Exam</a>
    <a href="javascript:void(0)" title="IAS Exam Plan">IAS Exam Plan</a>
    <a href="javascript:void(0)" title="Tips for UPCS">Tips for UPCS</a>
    <a href="javascript:void(0)" title="IAS Exam Time Table">IAS Exam Time Table</a>
    <a href="javascript:void(0)" title="राजव्यवस्था">राजव्यवस्था</a>
    <a href="javascript:void(0)" title="अर्थव्यवस्था">अर्थव्यवस्था </a>
    <a href="javascript:void(0)" title="पर्यावरण">पर्यावरण</a>
    <a href="javascript:void(0)" title="विज्ञान और प्रोद्योगिकी">विज्ञान और प्रोद्योगिकी</a>
    <a href="javascript:void(0)" title="कला संस्कृति">कला संस्कृति</a>
  </div>
</div>