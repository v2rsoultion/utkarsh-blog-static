    <!-- Header --> <?php include 'header.php';?>
    <!-- Blog Banner -->
    <section class="inner-page-banner">
      <div class="container">
        <div class="row">
          <div class="col-xl-12">
            <div>
              <h1>Blog</h1>
              <ul>
                <li title="Home"><a href="javascript:void(0)">Home</a></li>
                <li>/</li>
                <li title="Blog"><a href="javascript:void(0)">Blog</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Blog Banner -->
    <!-- Blog Section start -->
    <section class="blog-section">
      <div class="container">
        <div class="row">
          <div class="col-xl-8 col-lg-8 col-md-6">
            <div class="blog-heading">
              <img src="images/blog-low-img.jpg" alt="Image" class="img-fluid" />
              <div class="blog-detail-box">
                <ul>
                  <li><i class="fas fa-user"></i> Utkarsh</li>
                  <li><i class="far fa-calendar-alt"></i> February 1, 2020</li>
                  <li><i class="fas fa-comment-dots"></i> 2</li>
                  <li>
                    <i class="fas fa-external-link-square-alt"></i> Blog, Blog English, Other
                  </li>
                </ul>
                <h5> CLAT Eligibility 2020 – Age Limit, Academic Qualification </h5>
                <p> CLAT stands for Common Law Admission Test and is a national-level entrance exam conducted by the
                  Consortium of the National Law Universities of India to provide admissions in various law colleges in
                  the country. </p>
                <button title="Read More" type="button">Read more</button>
              </div>
            </div>
            <div class="featured-blog">
              <h2>Featured Posts</h2>
              <div class="blog-feature-box">
                <img src="images/blog-img8.jpg" alt="image" class="img-fluid" />
                <div class="feature-box-detail">
                  <ul>
                    <li><i class="fas fa-user"></i> Utkarsh</li>
                    <li>
                      <i class="far fa-calendar-alt"></i> February 1, 2020
                    </li>
                    <li><i class="fas fa-comment-dots"></i> 2</li>
                    <li>
                      <i class="fas fa-external-link-square-alt"></i> Blog, Blog English, Other
                    </li>
                  </ul>
                  <a href="javascript:void(0)" title="7 Unique Ways to Crack REET Exam in 1 Month">7 Unique Ways to
                    Crack REET Exam in 1 Month</a>
                  <p> Government is about to declare the REET examination date, it might be upcoming months. REET stands
                    for Rajasthan Eligibility Examination for Teachers and is a very sought-after exam in the state of
                    Rajasthan. </p>
                </div>
              </div>
              <div class="blog-feature-box">
                <img src="images/blog-img7.jpg" alt="image" class="img-fluid" />
                <div class="feature-box-detail">
                  <ul>
                    <li><i class="fas fa-user"></i> Utkarsh</li>
                    <li>
                      <i class="far fa-calendar-alt"></i> February 1, 2020
                    </li>
                    <li><i class="fas fa-comment-dots"></i> 2</li>
                    <li>
                      <i class="fas fa-external-link-square-alt"></i> Blog, Blog English, Other
                    </li>
                  </ul>
                  <a href="javascript:void(0)" title="7 Unique Ways to Crack REET Exam in 1 Month">7 Unique Ways to
                    Crack REET Exam in 1 Month</a>
                  <p> Government is about to declare the REET examination date, it might be upcoming months. REET stands
                    for Rajasthan Eligibility Examination for Teachers and is a very sought-after exam in the state of
                    Rajasthan. </p>
                </div>
              </div>
            </div>
            <div class="blog-links">
              <div class="row">
                <div class="col-lg-4 col-md-12">
                  <a href="javascript:void(0)" title="अंतर्राष्ट्रीय जनसंख्या और विकास सम्मेलन ICPD">
                    <img src="images/blog-img6.jpg" alt="image" class="img-fluid" />
                    <div class="link-detail">
                      <ul>
                        <li><i class="fas fa-user"></i> Utkarsh</li>
                        <li>
                          <i class="far fa-calendar-alt"></i> August 1, 2020
                        </li>
                      </ul>
                      <h5>अंतर्राष्ट्रीय जनसंख्या और विकास सम्मेलन ICPD</h5>
                    </div>
                  </a>
                </div>
                <div class="col-lg-4 col-md-12">
                  <a href="javascript:void(0)" title="जॉर्डन-इजराइल शांति संधि का अंत">
                    <img src="images/blog-img5.jpg" alt="image" class="img-fluid" />
                    <div class="link-detail">
                      <ul>
                        <li><i class="fas fa-user"></i> Utkarsh</li>
                        <li>
                          <i class="far fa-calendar-alt"></i> August 1, 2020
                        </li>
                      </ul>
                      <h5>जॉर्डन-इजराइल शांति संधि का अंत</h5>
                    </div>
                  </a>
                </div>
                <div class="col-lg-4 col-md-12">
                  <a href="javascript:void(0)" title="ओवरसीज सिटीजन ऑफ इंडिया- OCI">
                    <img src="images/blog-img4.jpg" alt="image" class="img-fluid" />
                    <div class="link-detail">
                      <ul>
                        <li><i class="fas fa-user"></i> Utkarsh</li>
                        <li>
                          <i class="far fa-calendar-alt"></i> August 1, 2020
                        </li>
                      </ul>
                      <h5>ओवरसीज सिटीजन ऑफ इंडिया- OCI</h5>
                    </div>
                  </a>
                </div>
              </div>
            </div>
            <div class="featured-blog">
              <h2>Must Read</h2>
              <div class="blog-feature-box">
                <img src="images/blog-img8.jpg" alt="image" class="img-fluid" />
                <div class="feature-box-detail">
                  <ul>
                    <li><i class="fas fa-user"></i> Utkarsh</li>
                    <li>
                      <i class="far fa-calendar-alt"></i> February 1, 2020
                    </li>
                    <li><i class="fas fa-comment-dots"></i> 2</li>
                    <li>
                      <i class="fas fa-external-link-square-alt"></i> Blog, Blog English, Other
                    </li>
                  </ul>
                  <a href="javascript:void(0)" title="7 Unique Ways to Crack REET Exam in 1 Month">7 Unique Ways to
                    Crack REET Exam in 1 Month</a>
                  <p> Government is about to declare the REET examination date, it might be upcoming months. REET stands
                    for Rajasthan Eligibility Examination for Teachers and is a very sought-after exam in the state of
                    Rajasthan. </p>
                </div>
              </div>
              <div class="blog-feature-box">
                <img src="images/blog-img7.jpg" alt="image" class="img-fluid" />
                <div class="feature-box-detail">
                  <ul>
                    <li><i class="fas fa-user"></i> Utkarsh</li>
                    <li>
                      <i class="far fa-calendar-alt"></i> February 1, 2020
                    </li>
                    <li><i class="fas fa-comment-dots"></i> 2</li>
                    <li>
                      <i class="fas fa-external-link-square-alt"></i> Blog, Blog English, Other
                    </li>
                  </ul>
                  <a href="javascript:void(0)" title="7 Unique Ways to Crack REET Exam in 1 Month">7 Unique Ways to
                    Crack REET Exam in 1 Month</a>
                  <p> Government is about to declare the REET examination date, it might be upcoming months. REET stands
                    for Rajasthan Eligibility Examination for Teachers and is a very sought-after exam in the state of
                    Rajasthan. </p>
                </div>
              </div>
              <div class="blog-feature-box">
                <img src="images/blog-img2.jpg" alt="image" class="img-fluid" />
                <div class="feature-box-detail">
                  <ul>
                    <li><i class="fas fa-user"></i> Utkarsh</li>
                    <li>
                      <i class="far fa-calendar-alt"></i> February 1, 2020
                    </li>
                    <li><i class="fas fa-comment-dots"></i> 2</li>
                    <li>
                      <i class="fas fa-external-link-square-alt"></i> Blog, Blog English, Other
                    </li>
                  </ul>
                  <a href="javascript:void(0)" title="केरल की फाइबर ऑप्टिक नेटवर्क परियोजना">केरल की फाइबर ऑप्टिक
                    नेटवर्क परियोजना</a>
                  <p> इस परियोजना के क्रियान्वयन से देश के सूचना एवं प्रौद्योगिकी उद्योग को मदद मिलने के साथ-साथ कृत्रिम
                    बुद्धिमत्ता (Artificial Intelligence), ब्लॉकचैन (Blockchain) तथा स्टार्टअप्स जैसे क्षेत्रों को विकास
                    के नए अवसर प्राप्त होंगे। </p>
                </div>
              </div>
              <div class="blog-feature-box">
                <img src="images/blog-img3.jpg" alt="image" class="img-fluid" />
                <div class="feature-box-detail">
                  <ul>
                    <li><i class="fas fa-user"></i> Utkarsh</li>
                    <li>
                      <i class="far fa-calendar-alt"></i> February 1, 2020
                    </li>
                    <li><i class="fas fa-comment-dots"></i> 2</li>
                    <li>
                      <i class="fas fa-external-link-square-alt"></i> Blog, Blog English, Other
                    </li>
                  </ul>
                  <a href="javascript:void(0)" title="भारत के राज्यों में राष्ट्रपति शासन">भारत के राज्यों में
                    राष्ट्रपति शासन</a>
                  <p> भारतीय संविधान के भाग 18 में आपात उपबंधों को तीन भागों में बाँटा गया है- राष्ट्रीय आपात
                    (अनुच्छेद-352), राज्यों में संवैधानिक तंत्र की विफलता/राष्ट्रपति शासन (अनुच्छेद-356) और वित्तीय आपात
                    (अनुच्छेद-360)। </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-6">
            <!-- blog sidebar --> <?php include 'blog-sidebar.php';?>
          </div>
        </div>
      </div>
    </section>
    <!-- Blog Section End -->
    <!-- Footer --> <?php include 'footer.php';?>