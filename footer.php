  <!-- Footer Start -->
  <section class="footer-section">
    <div class="container">
      <div class="row">
        <div class="col-xl-3 col-lg-3 col-md-3">
          <div class="address">
            <ul>
              <li>
                <a href="javascript:void(0)" title="Utkarsh Classes" class="mb-0 text-center"><img src="images/logo.svg"
                    alt="Logo" width="60" /></a>
              </li>
              <li>
                <i class="fas fa-map-marker-alt"></i>
                <span>Utkarsh classes, Vyas Bhawan, 1st A Road,Sardarpura, Jodhpur Rajathan, 342001, INDIA.</span>
              </li>
              <li>
                <i class="fas fa-phone"></i>
                <a href="tel:+91-911 669 1119" title="Phone">+91-9116691119</a>
              </li>
              <li>
                <i class="fas fa-envelope"></i>
                <a href="mailto:support@utkarsh.com" title="Mail">support@utkarsh.com</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-6">
          <div class="quick-links">
            <ul>
              <h5>Quick Links</h5>
              <li>
                <a href="javascript:void(0)" title="About Us">About Us</a>
              </li>
              <li>
                <a href="javascript:void(0)" title="News">News</a>
              </li>
              <li>
                <a href="javascript:void(0)" title="Career">Career</a>
              </li>
              <li>
                <a href="javascript:void(0)" title="Contact Us">Contact Us</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-xl-2 col-lg-3 col-md-3 col-6">
          <div class="quick-links support-links">
            <ul>
              <h5>Support</h5>
              <li>
                <a href="javascript:void(0)" title="Student Support">Student Support</a>
              </li>
              <li>
                <a title="FAQ's" href="javascript:void(0)">FAQ's</a>
              </li>
              <li>
                <a href="javascript:void(0)" title="Blog Hindi">Blog Hindi</a>
              </li>
              <li>
                <a href="javascript:void(0)" title="Blog English">Blog English</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-xl-4 col-lg-3 col-md-3">
          <div class="join-us">
            <ul>
              <h5>Join us on</h5>
              <li>
                <a href="https://facebook.com/theutkarshclasses" target="_blank" title="Facebook">
                  <i class="fab fa-facebook-f"></i>
                </a>
              </li>
              <li>
                <a href="https://twitter.com/utkarshjodhpur" target="_blank" title="Twitter">
                  <i class="fab fa-twitter"></i>
                </a>
              </li>
              <li>
                <a href="https://instagram.com/utkarshclasses" target="_blank" title="Instagram">
                  <i class="fab fa-instagram"></i>
                </a>
              </li>
              <li>
                <a href="https://t.me/utkarshjodhpur" target="_blank" title="Telegram">
                  <i class="fab fa-telegram-plane"></i>
                </a>
              </li>
              <li>
                <a href="https://www.youtube.com/playlist?list=PLoZP2WsNfBSFGsub-jJb_uNW58zQRpDXc" target="_blank"
                  title="Youtube">
                  <i class="fab fa-youtube"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="footer-app">
            <h5>Download our app</h5>
            <p>Ebooks, Test Preparation, Video Lectures</p>
            <button class="app-store-btn" title="Download on the Apple Store">
              <a href="https://www.apple.com/ios/app-store/" target="_blank"><img src="images/app.png"
                  alt="App store button" /></a>
            </button>
            <button class="app-store-btn" title="Get it from Google Play">
              <a href="https://play.google.com/store?hl=en" target="_blank">
                <img src="images/app1.png" alt="App store button" /></a>
            </button>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Copyright Section -->
  <section class="copyright-section">
    <div class="container">
      <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-12">
          <div class="copyright-left">
            <p> &copy; 2020 <span>Utkarsh Classes & Edutech Pvt. Ltd.</span> All Rights Reserved </p>
          </div>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-12">
          <div class="terms">
            <ul>
              <li>
                <a href="javascript:void(0)" title="Privacy Policy">Privacy Policy</a>
              </li>
              <li>
                <a href="javascript:void(0)" title="Refund Policy">Refund Policy</a>
              </li>
              <li>
                <a href="javascript:void(0)" title="Terms & Conditions">Terms & Conditions</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Footer End -->
  </body>
  <!-- JS, Popper.js, and jQuery -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <script src="js/typeahead.jquery.min.js"></script>
  <script src="js/bootstrapValidator.min.js"></script>
  <script>
// Navbar toggle
$(".navbar-toggler").click(function() {
  $(this).toggleClass("change");
});
// Navbar Fixed
$(window).scroll(function() {
  if ($(this).scrollTop() > 120) {
    $(".header-section").addClass("header-fixed");
  } else {
    $(".header-section").removeClass("header-fixed");
  }
});
//
$(document).ready(function() {
  //Reply comment toggle
  $(".replyForm").hide();
  $(".reply-comment").click(function() {
    $(this).closest(".comment-detail").find(".replyForm").toggle();
  });
  // Comment form validation
  $("#commentForm").bootstrapValidator({
    fields: {
      name: {
        validators: {
          notEmpty: {
            message: "Please Enter Name",
          },
        },
      },
      email: {
        validators: {
          notEmpty: {
            message: "Please Enter Your Email Address",
          },
        },
      },
      commentBox: {
        validators: {
          notEmpty: {
            message: "Please Enter Your Comment",
          },
        },
      },
    },
  });
});
  </script>

  </html>