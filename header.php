<!DOCTYPE html>
<html>

<head>
  <title>Utkarsh Classes</title>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1.0, user-scalable=no" />
  <link rel="icon" href="images/favicon.png" type="image/png" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
  <link rel="stylesheet preload" as="style" href="https://pro.fontawesome.com/releases/v5.8.2/css/all.css" />
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700;800&display=swap" rel="stylesheet" />
  <link
    href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&family=Open+Sans:wght@400;600;700;800&display=swap"
    rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/blog-header.css" />
  <link rel="stylesheet" type="text/css" href="css/blog-style.css" />
  <link rel="stylesheet" type="text/css" href="css/blog-responsive.css" />
</head>

<body>
  <!-- Header Start -->
  <section class="header-section">
    <nav class="navbar navbar-expand-lg">
      <!-- Navbar Brand -->
      <a class="navbar-brand" href="javascript:void(0)"><img src="images/logo.svg" alt="Logo" width="60" /></a>
      <!-- Mobile Right menu -->
      <div class="menu-right menu-right-responsive">
        <ul>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false" href="javascript:void(0)" title="Language"> ENGLISH </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="javascript:void(0)"> ENGLISH</a>
              <a class="dropdown-item" href="javascript:void(0)"> हिन्दी </a>
            </div>
          </li>
          <!-- <li>
              <button class="btn btn-get-started" title="Get Started">
                Get Started
              </button>
            </li> -->
        </ul>
      </div>
      <!-- Mobile Menu Bars -->
      <div class="menuResponsive-opener navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
      </div>
      <!-- Menu Itemes -->
      <div class="collapse navbar-collapse menuResponsive" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="javascript:void(0)" title="Home">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="javascript:void(0)" title="About Us">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="javascript:void(0)" title="Courses">Courses</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="javascript:void(0)" title="Blog">Blog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="javascript:void(0)" title="Student Support">Student Support</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="javascript:void(0)" title="Contact Us">Contact Us</a>
          </li>
        </ul>
        <ul class="nav-item d-inline-block menu-right">
          <li>
            <a href="tel:+91-911 669 1119" title="Phone"> +91-9116691119 </a>
          </li>
          <li>|</li>
          <li class="dropdown">
            <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="navbarDropdown" role="button"
              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> ENGLISH </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="javascript:void(0)">ENGLISH </a>
              <a class="dropdown-item" href="javascript:void(0)">हिन्दी </a>
            </div>
          </li>
        </ul>
        <!-- <button class="get-started-btn" type="button" title="Get Started">
            Get Started
          </button> -->
      </div>
    </nav>
  </section>
  <!-- Header End -->