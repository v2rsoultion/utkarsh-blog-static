    <!-- Header --> <?php include 'header.php';?>
    <!-- News Banner -->
    <section class="inner-page-banner">
      <div class="container">
        <div class="row">
          <div class="col-xl-12">
            <div>
              <h1>News</h1>
              <ul>
                <li title="Home"><a href="javascript:void(0)">Home</a></li>
                <li>/</li>
                <li title="News"><a href="javascript:void(0)">News</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End News Banner -->
    <!-- Blog Section start -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-xl-8 col-lg-8 col-md-6 news-section">
            <div class="row">
              <div class="col-lg-6">
                <a href="javascript:void(0)" title="Pradeep Singh Tops UPSC Civil Services Exam 2019" class="news-box">
                  <img src="images/news-img1.jpg" alt="image" class="img-fluid">
                  <div class="news-detail-box">
                    <h5>Pradeep Singh Tops UPSC Civil Services Exam 2019</h5>
                    <ul>
                      <li><i class="fas fa-user"></i> Utkarsh</li>
                      <li><i class="far fa-calendar-alt"></i> February 1, 2020</li>
                      <li><i class="fas fa-comment-dots"></i> 2</li>
                    </ul>
                  </div>
                </a>
              </div>
              <div class="col-lg-6">
                <a href="javascript:void(0)" title="Nirmal Gehlot inspires hope amidst COVID-19 crisis"
                  class="news-box">
                  <img src="images/news-img2.jpg" alt="image" class="img-fluid">
                  <div class="news-detail-box">
                    <h5>Nirmal Gehlot inspires hope amidst COVID-19 crisis</h5>
                    <ul>
                      <li><i class="fas fa-user"></i> Utkarsh</li>
                      <li><i class="far fa-calendar-alt"></i> February 1, 2020</li>
                      <li><i class="fas fa-comment-dots"></i> 2</li>
                    </ul>
                  </div>
                </a>
              </div>
              <div class="col-lg-6">
                <a href="javascript:void(0)" title="Rajasthan Cooperative Bank Exam Date 2019 Announced"
                  class="news-box">
                  <img src="images/news-img3.jpg" alt="image" class="img-fluid">
                  <div class="news-detail-box">
                    <h5>Rajasthan Cooperative Bank Exam Date 2019 Announced</h5>
                    <ul>
                      <li><i class="fas fa-user"></i> Utkarsh</li>
                      <li><i class="far fa-calendar-alt"></i> February 1, 2020</li>
                      <li><i class="fas fa-comment-dots"></i> 2</li>
                    </ul>
                  </div>
                </a>
              </div>
              <div class="col-lg-6">
                <a href="javascript:void(0)"
                  title="JE- कनिष्ठ अभियंता संयुक्त सीधी-भर्ती परीक्षा-2020 – 1098 पदों पर बंपर भर्ती" class="news-box">
                  <img src="images/news-img4.jpg" alt="image" class="img-fluid">
                  <div class="news-detail-box">
                    <h5>JE- कनिष्ठ अभियंता संयुक्त सीधी-भर्ती परीक्षा-2020 – 1098 पदों पर बंपर भर्ती</h5>
                    <ul>
                      <li><i class="fas fa-user"></i> Utkarsh</li>
                      <li><i class="far fa-calendar-alt"></i> February 1, 2020</li>
                      <li><i class="fas fa-comment-dots"></i> 2</li>
                    </ul>
                  </div>
                </a>
              </div>
              <div class="col-lg-6">
                <a href="javascript:void(0)" title="FCI 2019 Manager Phase I Admit Card Realesed" class="news-box">
                  <img src="images/news-img5.jpg" alt="image" class="img-fluid">
                  <div class="news-detail-box">
                    <h5>FCI 2019 Manager Phase I Admit Card Realesed</h5>
                    <ul>
                      <li><i class="fas fa-user"></i> Utkarsh</li>
                      <li><i class="far fa-calendar-alt"></i> February 1, 2020</li>
                      <li><i class="fas fa-comment-dots"></i> 2</li>
                    </ul>
                  </div>
                </a>
              </div>
              <div class="col-lg-6">
                <a href="javascript:void(0)" title="TSSPDCL recruitment 2019 application form started" class="news-box">
                  <img src="images/news-img6.jpg" alt="image" class="img-fluid">
                  <div class="news-detail-box">
                    <h5>TSSPDCL recruitment 2019 application form started</h5>
                    <ul>
                      <li><i class="fas fa-user"></i> Utkarsh</li>
                      <li><i class="far fa-calendar-alt"></i> February 1, 2020</li>
                      <li><i class="fas fa-comment-dots"></i> 2</li>
                    </ul>
                  </div>
                </a>
              </div>
              <div class="col-lg-6">
                <a href="javascript:void(0)" title="ISRO Scientist/Engineer Recruitment 2019" class="news-box">
                  <img src="images/news-img7.jpg" alt="image" class="img-fluid">
                  <div class="news-detail-box">
                    <h5>ISRO Scientist/Engineer Recruitment 2019</h5>
                    <ul>
                      <li><i class="fas fa-user"></i> Utkarsh</li>
                      <li><i class="far fa-calendar-alt"></i> February 1, 2020</li>
                      <li><i class="fas fa-comment-dots"></i> 2</li>
                    </ul>
                  </div>
                </a>
              </div>
              <div class="col-lg-6">
                <a href="javascript:void(0)" title="Rajasthan Assistant Engineer Main Exam Postponed" class="news-box">
                  <img src="images/news-img8.jpg" alt="image" class="img-fluid">
                  <div class="news-detail-box">
                    <h5>Rajasthan Assistant Engineer Main Exam Postponed</h5>
                    <ul>
                      <li><i class="fas fa-user"></i> Utkarsh</li>
                      <li><i class="far fa-calendar-alt"></i> February 1, 2020</li>
                      <li><i class="fas fa-comment-dots"></i> 2</li>
                    </ul>
                  </div>
                </a>
              </div>
              <div class="col-lg-6">
                <a href="javascript:void(0)"
                  title="JE- कनिष्ठ अभियंता संयुक्त सीधी-भर्ती परीक्षा-2020 – 1098 पदों पर बंपर भर्ती" class="news-box">
                  <img src="images/news-img4.jpg" alt="image" class="img-fluid">
                  <div class="news-detail-box">
                    <h5>JE- कनिष्ठ अभियंता संयुक्त सीधी-भर्ती परीक्षा-2020 – 1098 पदों पर बंपर भर्ती</h5>
                    <ul>
                      <li><i class="fas fa-user"></i> Utkarsh</li>
                      <li><i class="far fa-calendar-alt"></i> February 1, 2020</li>
                      <li><i class="fas fa-comment-dots"></i> 2</li>
                    </ul>
                  </div>
                </a>
              </div>
              <div class="col-lg-6">
                <a href="javascript:void(0)" title="FCI 2019 Manager Phase I Admit Card Realesed" class="news-box">
                  <img src="images/news-img5.jpg" alt="image" class="img-fluid">
                  <div class="news-detail-box">
                    <h5>FCI 2019 Manager Phase I Admit Card Realesed</h5>
                    <ul>
                      <li><i class="fas fa-user"></i> Utkarsh</li>
                      <li><i class="far fa-calendar-alt"></i> February 1, 2020</li>
                      <li><i class="fas fa-comment-dots"></i> 2</li>
                    </ul>
                  </div>
                </a>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-6">
            <!-- blog sidebar --> <?php include 'blog-sidebar.php';?>
          </div>
        </div>
      </div>
    </section>
    <!-- News Section End -->
    <!-- Footer --> <?php include 'footer.php';?>